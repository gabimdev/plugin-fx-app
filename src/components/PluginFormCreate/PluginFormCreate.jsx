import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createPlugin } from '../../api/plugins';
import { handleInput, resetForm } from '../../redux/slices/form.slice';
import { addPlugin } from '../../redux/slices/plugins.slice';
import 'bootstrap/dist/css/bootstrap.css';
import './PluginFormCreate.scss';

const PluginFormCreate = (props) => {
    const form = useSelector((state) => state.form);
    console.log(form);
    const [image, setImage] = useState(null);
    const dispatch = useDispatch();

    const submit = async (ev) => {
        ev.preventDefault();
        const formData = new FormData();
        for (let key in form) {
            formData.append(key, form[key]);
            console.log(key, form[key]);
        }
        if (image) {
            formData.append('image', image);
        }
        const pluginCreated = await createPlugin(formData);
        dispatch(addPlugin(pluginCreated));
        dispatch(resetForm());
        props.history.push('/plugins');
    };

    const inputChange = (ev) => {
        const { name, value, type, files } = ev.target;
        if (type === 'file') {
            setImage(files[0]);
        } else {
            dispatch(handleInput({ name, value }));
        }
    };

    return (
        <div className="w-75 p-3 shadow p-3 mb-5 bg-body rounded form-container">
            <form onSubmit={submit} encType="multipart/form-data" method="POST">
                <div className="d-flex justify-content-around form-container__group">
                    <div className="form-group form-container__input">
                        <label htmlFor="name">Plugin Name</label>
                        <input
                            type="text"
                            className="form-control"
                            id="name"
                            name="name"
                            placeholder="Plugin Name"
                            value={form.name}
                            onChange={inputChange}
                        />
                    </div>
                    <div className="form-group from-container__input">
                        <label htmlFor="developer">Developer</label>
                        <input
                            type="text"
                            className="form-control"
                            id="developer"
                            name="developer"
                            placeholder="Developer"
                            value={form.developer}
                            onChange={inputChange}
                        />
                    </div>
                    <div className="form-group form-container__input">
                        <label htmlFor="image">Image</label>
                        <input
                            type="file"
                            className="form-control"
                            id="image"
                            name="image"
                            onChange={inputChange}
                        />
                    </div>
                </div>
                {/* Selectors */}
                <div className="d-flex justify-content-around form-container__group">
                    <div className="form-group">
                        <label htmlFor="comercial">Comercial</label>
                        <select
                            className="form-control"
                            id="exampleFormControlSelect1"
                            name="comercial"
                            value={form.comercial}
                            onChange={inputChange}
                        >
                            <option> </option>
                            <option>free</option>
                            <option>paid</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="format">Format</label>
                        <select
                            className="form-control"
                            id="format"
                            name="format"
                            value={form.format}
                            onChange={inputChange}
                        >
                            <option> </option>
                            <option>VST</option>
                            <option>VST3</option>
                            <option>AU</option>
                            <option>AAX</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="platform">Platform</label>
                        <select
                            className="form-control"
                            id="platform"
                            name="platform"
                            value={form.platform}
                            onChange={inputChange}
                        >
                            <option> </option>
                            <option>windows</option>
                            <option>mac</option>
                            <option>iOS</option>
                            <option>andoid</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="category">Category</label>
                        <select
                            className="form-control"
                            id="category"
                            name="category"
                            value={form.category}
                            onChange={inputChange}
                        >
                            <option> </option>
                            <option>dynamic</option>
                            <option>modulation</option>
                            <option>time-based</option>
                            <option>spectral</option>
                            <option>filter</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="pluginType">Plugin Type</label>
                        <select
                            className="form-control"
                            id="pluginType"
                            name="pluginType"
                            value={form.pluginType}
                            onChange={inputChange}
                        >
                            <option> </option>
                            <option>chorus</option>
                            <option>tremolo</option>
                            <option>flanger</option>
                            <option>phaser</option>
                            <option>reverb</option>
                            <option>delay</option>
                            <option>echo</option>
                            <option>eq</option>
                            <option>pannig</option>
                            <option>compression</option>
                            <option>distortion</option>
                            <option>filter</option>
                            <option>gater</option>
                            <option>limiter</option>
                            <option>pitch shifter</option>
                            <option>saturation</option>
                            <option>vibrato</option>
                            <option>vocoder</option>
                            <option>amp simulator</option>
                            <option>bit crusher</option>
                            <option>channel strip</option>
                            <option>exciter</option>
                            <option>mastering</option>
                        </select>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="description">Description</label>
                    <textarea
                        className="form-control"
                        rows="3"
                        id="description"
                        name="description"
                        placeholder="description"
                        value={form.description}
                        onChange={inputChange}
                    ></textarea>
                </div>
                <button type="submit" className="btn btn-primary btn-create">
                    Create New Plugin
                </button>
            </form>
        </div>
    );
};

export default PluginFormCreate;
