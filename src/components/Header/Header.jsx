import { useDispatch, useSelector } from 'react-redux';
import { logoutAsync } from '../../redux/slices/user.slice';
import { handleInput } from '../../redux/slices/search.slice';
import { Link, useHistory } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './Header.scss';

const Header = (props) => {
    const dispatch = useDispatch();
    const { user } = useSelector((state) => state.user);

    const history = useHistory();
    console.log(history);

    const inputChange = (ev) => {
        const { value } = ev.target;
        dispatch(handleInput({ value }));
        history.push('/plugins');
    };

    const logout = () => {
        dispatch(logoutAsync());
        history.push('/');
    };

    return (
        <header className="p-3 bg-dark text-white w-100 p-3">
            <div className="container">
                <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
                    <Link to="/" className="nav-link px-2 text-white">
                        Plugin Fx
                    </Link>
                    <ul className="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                        <li>
                            <Link
                                to="/"
                                className="nav-link px-2 text-secondary"
                            >
                                Home
                            </Link>
                        </li>
                        <li>
                            <Link
                                to="/plugins"
                                className="nav-link px-2 text-white"
                            >
                                Plugins
                            </Link>
                        </li>
                        <li>
                            {user && (
                                <Link
                                    to="/create"
                                    className="nav-link px-2 text-white"
                                >
                                    Create New Plugin
                                </Link>
                            )}
                        </li>
                    </ul>
                    <form className="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
                        <input
                            type="search"
                            className="form-control form-control-dark"
                            placeholder="Search by Name"
                            aria-label="Search"
                            onChange={inputChange}
                        />
                    </form>
                    <div className="text-end">
                        {user ? (
                            <div>
                                <span className="nav__text">
                                    Hello, {user.username}
                                </span>
                                <button
                                    className="btn btn-danger"
                                    onClick={logout}
                                >
                                    Logout
                                </button>
                            </div>
                        ) : (
                            <div>
                                <Link to="/login">
                                    <button
                                        type="button"
                                        className="btn btn-outline-light me-2"
                                    >
                                        Login
                                    </button>
                                </Link>
                                <Link to="/register">
                                    <button
                                        type="button"
                                        className="btn btn-warning"
                                    >
                                        Sign-up
                                    </button>
                                </Link>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;
