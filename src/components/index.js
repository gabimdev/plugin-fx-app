import Header from './Header/Header';
import LoginForm from './LoginForm/LoginForm';
import Register from './Register/Register';
import Footer from './Footer/Footer';
import Home from './Home/Home';
import PluginsView from './PluginsView/PluginsView';
import PluginFormCreate from './PluginFormCreate/PluginFormCreate';
import SecureRoute from './SecureRoute/SecureRoute';
import PluginDetail from './PluginDetail/PluginDetail';

export {
    Header,
    LoginForm,
    Register,
    Footer,
    Home,
    PluginsView,
    PluginFormCreate,
    SecureRoute,
    PluginDetail,
}