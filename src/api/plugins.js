import { serverDirection } from './auth';

const allPluginsUrl = `${serverDirection}/plugins/all`;
const createPluginUrl = `${serverDirection}/plugins/create`;

export const getAllPlugins = async () => {
    const request = await fetch(allPluginsUrl, {
        method: 'GET',
        mode: 'no-cors',
    });
    const response = await request.json();

    if (!request.ok) {
        throw new Error(response.message);
    }

    return response;
};

export const createPlugin = async (plugin) => {
    const request = await fetch(createPluginUrl, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Access-Control-Allow-Origin': '*',
        },
        credentials: 'include',
        withCredentials: true,
        body: plugin,
    });
    const response = await request.json();
    return response;
};
