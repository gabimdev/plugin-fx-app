import { createSlice } from '@reduxjs/toolkit';

const INTIAL_STATE = {
    name: '',
    developer: '',
    description: '',
    platform: '',
    format: '',
    comercial: '',
    category: '',
    pluginType: '',
};

export const formSlice = createSlice({
    name: 'form',
    initialState: INTIAL_STATE,
    reducers: {
        handleInput: (state, action) => {
            const { name, value } = action.payload;
            state[name] = value;
        },
        resetForm: (state, action)=>{
            state= {...INTIAL_STATE};
        }
    },
});

export const { handleInput, resetForm } = formSlice.actions;
